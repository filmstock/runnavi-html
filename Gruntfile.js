var module;
module.exports = function(grunt) {
  "use strict";
  var pkg = grunt.file.readJSON('package.json');

  grunt.initConfig({
    watch: {
      options: {
        livereload: true
      },
      html: {
        files: ['files/**/*.html'],
        //tasks: ['']
      },
      sass: {
        files: ['files/sass/*.scss'],
        tasks: ['compass','csscomb']
      },
      coffee: {
        tasks: 'coffee',
        files: ['files/src/**/*.coffee']
      }
    },
    compass: {
      dist: {
        options: {
          config: 'config.rb'
        }
      }
    },
    csscomb: {
      dev:{
        expand: true,
        cwd: 'files/css/',
        src: ['*.css'],
        dest: 'files/css/'
      }
    },
    cmq: {
      options:{
        log: false
      },
      dev: {
        files: {
          'files/css/': ['files/css/style.css']
        }
      }
    },
    connect: {
      livereload: {
        options: {
          port: 9000,
          base: './files/'
        }
      }
    },
    coffee: {
      compile: {
        files:[{
          expand: true,
          cwd: 'files/src/',
          src: ['files/**/*.coffee'],
          dest: 'files/dest/',
          ext: '.js',
        }]
      }
    },
    open: {
      server: {
        path: 'http://localhost:<%= connect.livereload.options.port %>'
      }
    }
  });

  var taskName;
    for(taskName in pkg.devDependencies) {
        if(taskName.substring(0, 6) === 'grunt-') {
            grunt.loadNpmTasks(taskName);
        }
    }
  grunt.registerTask('default',["connect","open","watch"]);
};
