(function() {
  var add, res;

  add = function(x, y) {
    return x + y;
  };

  res = add(5, 5);

  console.log(res);

}).call(this);
